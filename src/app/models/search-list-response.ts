import { ISearchResult } from '@models/search-result';

export interface ISearchListResponse {
  kind: string;
  etag: string;
  nextPageToken: string;
  regionCode: string;
  pageInfo: IPageInfo;
  items: ISearchResult[];
}

export interface IPageInfo {
  totalResults: number;
  resultsPerPage: number;
}
