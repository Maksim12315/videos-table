export interface IGrigItemInfo {
  thumbnailUrl: string;
  publishedAt: string;
  title: IVideoTitle;
  description: string;
}

export interface IVideoTitle {
  videoTitle: string;
  videoId: string;
}
