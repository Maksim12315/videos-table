import { Component } from '@angular/core';
import { IToolPanel, IToolPanelParams } from '@ag-grid-enterprise/all-modules';
import { ThumbnailComponent } from '@components/cells/thumbnail/thumbnail.component';
import { TitleComponent } from '@components/cells/title/title.component';
import { PublishedComponent } from '@components/cells/published/published.component';
import { DescriptionComponent } from '@components/cells/description/description.component';
import { CheckboxComponent } from '@components/cells/checkbox/checkbox.component';

@Component({
  selector: 'yvg-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements IToolPanel {

  selectedCount: number;
  allCount: number;
  params: IToolPanelParams;

  private readonly basicComponents = [
    {
      headerName: '',
      field: 'thumbnailUrl',
      cellRendererFramework: ThumbnailComponent,
      autoHeight: true
    },
    {
      headerName: 'Published on',
      field: 'publishedAt',
      cellRendererFramework: PublishedComponent
    },
    {
      headerName: 'Video Title',
      field: 'title',
      cellRendererFramework: TitleComponent
    },
    {
      headerName: 'Description',
      field: 'description',
      cellRendererFramework: DescriptionComponent,
      autoHeight: true
    }
  ];

  agInit(params: IToolPanelParams) {
    this.params = params;
    this.params.api.addEventListener('modelUpdated', this.updateTotals.bind(this));
    this.params.api.addEventListener('rowSelected', this.updateTotals.bind(this));
  }

  refresh() {}

  updateTotals(): void {
    this.selectedCount = 0;
    this.allCount = 0;
    this.params.api.forEachNode((rowNode) => {
      if (rowNode.isSelected()) {
        this.selectedCount++;
      }
      this.allCount++;
    });
  }

  onShowCheckboxesChange(event) {
    if (event.target.checked) {
      this.params.api.setColumnDefs(
        [
          {
            headerName: '',
            field: 'checkbox',
            cellRendererFramework: CheckboxComponent,
            headerComponent: 'selectAllComponent'
          },
          ...this.basicComponents
        ]
      );
    } else {
      this.params.api.setColumnDefs(
        [
          {
            headerName: '',
            field: 'thumbnailUrl',
            cellRendererFramework: ThumbnailComponent,
            autoHeight: true
          },
          {
            headerName: 'Published on',
            field: 'publishedAt',
            cellRendererFramework: PublishedComponent
          },
          {
            headerName: 'Video Title',
            field: 'title',
            cellRendererFramework: TitleComponent
          },
          {
            headerName: 'Description',
            field: 'description',
            cellRendererFramework: DescriptionComponent,
            autoHeight: true
          }
        ]
      );
    }
  }

}
