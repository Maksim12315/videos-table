import { Component, ChangeDetectionStrategy } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'yvg-published',
  templateUrl: './published.component.html',
  styleUrls: ['./published.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublishedComponent {

  params: any;

  agInit(params: any) {
    this.params = params;
  }

  get formattedDate(): string {
    return moment((this.params.value as string)).format('DD-MMM-YYYY');
  }

}
