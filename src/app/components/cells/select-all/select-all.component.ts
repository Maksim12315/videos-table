import { Component } from '@angular/core';

@Component({
  selector: 'yvg-select-all',
  templateUrl: './select-all.component.html',
  styleUrls: ['./select-all.component.scss']
})
export class SelectAllComponent {

  params: any;
  isChecked: boolean;

  agInit(params: any) {
    this.params = params;
    if (this.params && this.params.api) {
      this.isChecked = this.isAllLinesSelected();
    }
    // that when all the lines are selected, the flag is updated too
    this.params.api.addEventListener('rowSelected', this.update.bind(this));
  }

  selectAll() {
    this.isChecked ? this.params.api.deselectAll() : this.params.api.selectAll();
  }

  update() {
    this.isChecked = this.isAllLinesSelected();
  }

  private isAllLinesSelected(): boolean {
    return this.params.api.getSelectedRows().length === this.params.api.getDisplayedRowCount();
  }
}
