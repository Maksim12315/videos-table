import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'yvg-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DescriptionComponent {

  params: any;

  agInit(params: any) {
    this.params = params;
  }

}
