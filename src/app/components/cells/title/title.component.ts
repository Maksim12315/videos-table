import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'yvg-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TitleComponent {

  params: any;

  agInit(params: any) {
    this.params = params;
  }

}
