import { Component } from '@angular/core';

@Component({
  selector: 'yvg-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.scss']
})
export class ThumbnailComponent {
  params: any;

  agInit(params: any) {
    this.params = params;
  }

}
