import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoGridComponent } from '@components/video-grid/video-grid.component';
import { VideoService } from '@services/video.service';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { of } from 'rxjs';
import { TitleComponent } from '@components/cells/title/title.component';
import { ThumbnailComponent } from '@components/cells/thumbnail/thumbnail.component';
import { DescriptionComponent } from '@components/cells/description/description.component';
import { PublishedComponent } from '@components/cells/published/published.component';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
import { CheckboxComponent } from '@components/cells/checkbox/checkbox.component';

class VideoServiceMock {
  getVideosList() {
    return of([]);
  }
}

describe('VideoGridComponent', () => {
  let component: VideoGridComponent;
  let fixture: ComponentFixture<VideoGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        VideoGridComponent,
        TitleComponent,
        ThumbnailComponent,
        DescriptionComponent,
        PublishedComponent,
        ToolbarComponent,
        CheckboxComponent
      ],
      providers: [
        {provide: VideoService, useClass: VideoServiceMock},
      ],
      imports: [
        FormsModule,
        AgGridModule.withComponents([
          TitleComponent,
          ThumbnailComponent,
          DescriptionComponent,
          PublishedComponent,
          ToolbarComponent,
          CheckboxComponent
        ])
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
