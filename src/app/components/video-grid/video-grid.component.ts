import { Component, OnInit } from '@angular/core';
import { VideoService } from '@services/video.service';
import { ThumbnailComponent } from '@components/cells/thumbnail/thumbnail.component';
import { TitleComponent } from '@components/cells/title/title.component';
import { DescriptionComponent } from '@components/cells/description/description.component';
import { PublishedComponent } from '@components/cells/published/published.component';
import { AllModules } from '@ag-grid-enterprise/all-modules';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
import { GridOptions } from 'ag-grid-community';
import { SelectAllComponent } from '@components/cells/select-all/select-all.component';
import { first } from 'rxjs/operators';

@Component({
  selector: 'yvg-video-grid',
  templateUrl: './video-grid.component.html',
  styleUrls: ['./video-grid.component.scss']
})
export class VideoGridComponent implements OnInit {

  frameworkComponents;
  modules = AllModules;
  selectedAll: boolean;
  sideBar;

  columnDefs = [
    {
      headerName: '',
      field: 'thumbnailUrl',
      cellRendererFramework: ThumbnailComponent,
      autoHeight: true
    },
    {
      headerName: 'Published on',
      field: 'publishedAt',
      cellRendererFramework: PublishedComponent
    },
    {
      headerName: 'Video Title',
      field: 'title',
      cellRendererFramework: TitleComponent
    },
    {
      headerName: 'Description',
      field: 'description',
      cellRendererFramework: DescriptionComponent,
      autoHeight: true
    }
  ];

  gridOptions: GridOptions = {
    columnDefs: this.columnDefs,
    rowSelection: 'multiple',
    rowMultiSelectWithClick: true
  };

  constructor(
    private videoService: VideoService
  ) { }

  ngOnInit() {
    this.videoService.getVideosList().pipe(
      first()
    ).subscribe(res => {
      this.gridOptions.api.setRowData(res);
    });
    this.createSidebar();
  }

  getContextMenuItems(params) {
    if (params.column.colDef.field === 'title') {
      return [
        {
          name: `Open in new tab ${params.column.colDef.headerName}`,
          action: () => {
            window.open(`https://www.youtube.com/watch?v=${params.value.videoId}`, '_blank');
          }
        },
      ];
    }
  }

  createSidebar() {
    this.sideBar = {
      toolPanels: [
        {
          id: 'selectionMode',
          labelDefault: 'Toolbar',
          labelKey: 'selectionMode',
          toolPanel: 'toolbar'
        }
      ],
    };
    this.frameworkComponents = {
      toolbar: ToolbarComponent,
      selectAllComponent: SelectAllComponent
    };
  }

}
