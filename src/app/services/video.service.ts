import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ISearchListResponse } from '@models/search-list-response';
import { map } from 'rxjs/operators';
import { IGrigItemInfo } from '@models/grid-info';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private readonly endpoint: string;

  constructor(private httpClient: HttpClient) { }

  getVideosList(): Observable<IGrigItemInfo[]> {
    return (this.httpClient.get(
      // tslint:disable-next-line: max-line-length
      'https://www.googleapis.com/youtube/v3/search?key=AIzaSyDAPX_dm7GCBBpUKMAYA1PXI3E5jT-E0Rs&maxResults=50&type=video&part=snippet&q=john'
    ) as  Observable<ISearchListResponse>).pipe(
      map((res) => (
        res.items.map(item => ({
          thumbnailUrl: item.snippet.thumbnails.default.url,
          publishedAt: item.snippet.publishedAt,
          title: {
            videoTitle: item.snippet.title,
            videoId: item.id.videoId
          },
          description: item.snippet.description
        }))
      ))
    );
  }

}
