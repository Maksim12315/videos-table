import { TestBed } from '@angular/core/testing';

import { VideoService } from './video.service';
import { HttpClient } from '@angular/common/http';

class HttpClientMock {
  get() {}
}

describe('VideoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{provide: HttpClient, useClass: HttpClientMock}]
  }));

  it('should be created', () => {
    const service: VideoService = TestBed.get(VideoService);
    expect(service).toBeTruthy();
  });
});
