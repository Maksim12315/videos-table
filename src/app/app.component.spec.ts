import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { VideoGridComponent } from '@components/video-grid/video-grid.component';
import { AgGridModule, AgGridAngular } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { TitleComponent } from '@components/cells/title/title.component';
import { ThumbnailComponent } from '@components/cells/thumbnail/thumbnail.component';
import { DescriptionComponent } from '@components/cells/description/description.component';
import { PublishedComponent } from '@components/cells/published/published.component';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
import { CheckboxComponent } from '@components/cells/checkbox/checkbox.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach((async () => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        VideoGridComponent,
        TitleComponent,
        ThumbnailComponent,
        DescriptionComponent,
        PublishedComponent,
        ToolbarComponent,
        CheckboxComponent
      ],
      imports: [
        FormsModule,
        HttpClientModule,
        AgGridModule.withComponents([
          TitleComponent,
          ThumbnailComponent,
          DescriptionComponent,
          PublishedComponent,
          ToolbarComponent,
          CheckboxComponent
        ])
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });
});
