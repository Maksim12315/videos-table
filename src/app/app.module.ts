import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from '@app/app.component';
import { AgGridModule } from 'ag-grid-angular';
import { VideoGridComponent } from '@components/video-grid/video-grid.component';
import { VideoService } from '@services/video.service';
import { HttpClientModule } from '@angular/common/http';
import { TitleComponent } from '@components/cells/title/title.component';
import { ThumbnailComponent } from '@components/cells/thumbnail/thumbnail.component';
import { DescriptionComponent } from '@components/cells/description/description.component';
import { PublishedComponent } from '@components/cells/published/published.component';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
import { CheckboxComponent } from '@components/cells/checkbox/checkbox.component';
import { SelectAllComponent } from './components/cells/select-all/select-all.component';

@NgModule({
  declarations: [
    AppComponent,
    VideoGridComponent,
    TitleComponent,
    ThumbnailComponent,
    DescriptionComponent,
    PublishedComponent,
    ToolbarComponent,
    CheckboxComponent,
    SelectAllComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgGridModule.withComponents([
      TitleComponent,
      ThumbnailComponent,
      DescriptionComponent,
      PublishedComponent,
      ToolbarComponent,
      CheckboxComponent,
      SelectAllComponent
    ])
  ],
  providers: [
    VideoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
